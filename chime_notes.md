

<img src='https://static.chimeroi.com/project/help-center/help-center-logo.svg' />
 

1. Harness the power of intent targeting and social info to identify and attract buyers and sellers.

2. Rely on cutting-edge AI to automatically optimize your advertising strategy.

3. Take the guess work out of high frequency bidding, keyword selection, and creative planning.

```
Potential Leads View Our Ads

Potential Leads Visit IDX Website

Leads Register on the IDX Website

Registration via Facebook Lead Form

Lead Info and Activities Are Imported into Your CRM

Welcome Email Sent to Leads and Lead Alerts Sent to Agents
```

## High-Value Lead Capture
Our IDX sites keep lead capture as a key focus with various lead capture tools.

## SEO Ready
Stand out in web searches with your SEO-friendly IDX site. With speedy load times and MLS updates every 30 minutes our sites are designed with an SEO focus from the start.

## Designed for Lead Capture
We offer Home Evaluation, Schedule a Tour, Mortgage Calculator, and more. All features are actively tracked to give insight on lead preferences.

>A Chime Smart Plan is a robust lead nurturing feature that combines email drip campaigns with tasks to help you communicate with your leads on a continuous basis.If you choose to add a call/text package to your account, you can also build auto-texts into your campaigns. Because Smart Plans are so feature-rich, it is best to describe how to use them via video feature review. Please watch the video below, but note that it does last 45 minutes as it was pulled from a hands-on training being given to Chime users. This will be a great resource get started with Chime Smart Plans:

## List of FAQs

1. If there are text messages including in a Smart Plan and the user reaches their daily text limit for the day, does the Smart Plan pause or does the text simply fail to send?
2. How do I exclude current leads in the database from being added to Smart Plans?
3. Is there a way to send the first auto email/text during a specific time range?
4. Is there a way to resubscribe leads to Smart Plans if the lead has accidentally unsubscribed?
5. Is there a limit to the number of Smart Plan steps that can be created?
6. When building a behavior-based Smart Plan using certain triggering behaviors, will the Smart Plan be triggered every time a lead leaves a message, saves a listing, etc.? 
7. When editing a Smart Plan, what happens to Smart Plan-generated tasks that have not yet been marked as complete?
8. If my Smart Plan is set up to auto apply to leads but this isn't happening, what are some of the reasons this could be the case?
9, Why were texts/emails not sent out in the Smart Plan like they should have been?
10. Why is the Smart Plan paused with the error "Too many mail rejected"?
11. Can I copy/paste variables or do they have to be added via the drop-down menu?
12. If the system has auto-paused Smart Plans, is there a way to restart them?
13. Can I have one Smart Plan trigger multiple times for closing dates? 
14. How do I use email and text variables?
15. How to the "Conditional Questions" work?
16. Can I use a Smart Plan to send out a monthly email newsletter?
17. For application conditions, why can I choose multiple pipeline options for "when specified leads are newly created"?
18. Can I Auto Pause When The Lead's Pipeline Changes Automatically?
19. Can I Schedule a Smart Plan Step Within a Time Range?
20. Can I Include a Leads Family Member's Email as Well?
